import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans, AffinityPropagation, SpectralClustering
from sklearn.metrics import silhouette_score, jaccard_score
from sklearn.manifold import TSNE
from torchvision import datasets


def generate_dataset1():
    dataset1 = datasets.MNIST('./data', train=True, download=True)
    return dataset1.data.reshape(-1, 28*28).numpy()[:6000], dataset1.targets.numpy()[:6000]


def generate_dataset2():
    dataset1 = datasets.FashionMNIST('./data', train=True, download=True)
    return dataset1.data.reshape(-1, 28*28).numpy()[:6000], dataset1.targets.numpy()[:6000]


def average_dists_from_centers(data, cluster_centers, closest_clusters):
    k = len(cluster_centers)
    dist = 0
    for i, d in enumerate(data):
        nearest_cluster_coords = cluster_centers[closest_clusters[i]]
        dist += np.linalg.norm(d - nearest_cluster_coords, ord=2)
    return dist / len(data)


def jaccard_index(gt, pred):
    a = 0
    b = 0
    c = 0
    for i in range(len(pred)):
        for j in range(len(pred)):
            if pred[i] == pred[j] and gt[i] == gt[j]:
                a += 1
            elif pred[i] != pred[j] and gt[i] == gt[j]:
                b += 1
            elif pred[i] == pred[j] and gt[i] != gt[j]:
                c += 1
    return a / (a + b + c)


def apply(data, labels):
    print(data.shape)
    # Data can be used directly in KMeans. Uses euclidean distances internally.
    kmeans = KMeans(n_clusters=10, init='k-means++')
    kmeans.fit(data)
    c = kmeans.cluster_centers_
    x_kmeans = kmeans.predict(data)
    avg_dist = average_dists_from_centers(data, c, x_kmeans)
    print("KMeans avg. cluster distance: \t" + str(avg_dist))
    print("KMeans silhouette score: \t" + str(silhouette_score(data, x_kmeans)))
    print("KMeans jaccard index: \t" + str(jaccard_index(labels, x_kmeans)))

    # Adjacency matrix is computed via the "pairwise_kernels" function, using rbf kernel. This is needed, SC can't be
    # directly applied to the features.
    sc = SpectralClustering(n_clusters=10, affinity="nearest_neighbors")
    x_sc = sc.fit_predict(data)
    print("SC silhouette score: \t\t" + str(silhouette_score(data, x_sc)))
    print("SC jaccard index: \t" + str(jaccard_index(labels, x_sc)))

    # The required similarity matrix is computed via the "euclidean_distances" function and negated.
    # Since we are in R², euclidean distances are the most intuitive measure.
    ap = AffinityPropagation(random_state=None)
    ap.fit(data)
    
    c = ap.cluster_centers_
    if len(c) > 0:
        x_ap = ap.predict(data)
        avg_dist = average_dists_from_centers(data, c, x_ap)
        print("AP avg. cluster distance: \t" + str(avg_dist))
        print("AP silhouette score: \t\t" + str(silhouette_score(data, x_ap)))
        print("AP jaccard index: \t" + str(jaccard_index(labels, x_ap)))
    else:
        print("Error! AP did not converge.")


if __name__ == '__main__':
    data1, labels1 = generate_dataset1()
    data2, labels2 = generate_dataset2()
    print("MNIST")
    apply(data1, labels1)
    print("-------------------")
    print("Fashion MNIST")
    apply(data2, labels2)
    print("-------------------")
    print("After TSNE")
    print("MNIST")
    apply(TSNE(n_components=3).fit_transform(data1), labels1)
    print("-------------------")
    print("Fashion MNIST")
    apply(TSNE(n_components=3).fit_transform(data2), labels2)
    