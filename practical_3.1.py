import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans, AffinityPropagation, SpectralClustering
from sklearn.metrics import silhouette_score, jaccard_score

def generate_dataset1():
    data = []
    labels = []
    data.append(np.random.randn(20,2) + np.array([-1,-1]) * 5)
    labels.append(np.zeros(20,))
    for i in range(3):
        for j in range(3):
            data.append(np.random.randn(20,2) + np.array([i,j]) * 5)
            labels.append(np.ones(20,) * (1 + i * 3 + j))
    return np.concatenate(data), np.concatenate(labels).astype(int)
    
def generate_dataset2():
    k = 10
    data = []
    labels = []
    for i in range(k):
        data.append(np.random.randn(20,2) + np.array([np.cos(2 * np.pi * i / k), np.sin(2 * np.pi * i / k)]) * 9)
        labels.append(np.ones(20,) * i)
    return np.concatenate(data), np.concatenate(labels).astype(int)
    
    
def average_dists_from_centers(data, cluster_centers, closest_clusters):
    k = len(cluster_centers)
    dist = 0
    for i, d in enumerate(data):
        nearest_cluster_coords = cluster_centers[closest_clusters[i]]
        dist += np.linalg.norm(d - nearest_cluster_coords, ord=2)
    return dist / len(data)
        
def jaccard_index(gt, pred):
    pairs = []
    for i in range(len(pred)):
        for j in range(len(pred)):
            if i == j:
                continue
            pairs.append((i,j))
    a = 0
    b = 0
    c = 0
    for pair in pairs:
        if pred[pair[0]] == pred[pair[1]] and gt[pair[0]] == gt[pair[1]]:
            a += 1
        elif pred[pair[0]] != pred[pair[1]] and gt[pair[0]] == gt[pair[1]]:
            b += 1
        elif pred[pair[0]] == pred[pair[1]] and gt[pair[0]] != gt[pair[1]]:
            c += 1
    return a / (a + b + c)
            

def apply(data, labels):
    plt.scatter(data[:,0], data[:,1])

    # Data can be used directly in KMeans. Uses euclidean distances internally.
    kmeans = KMeans(n_clusters=10, init='k-means++')
    kmeans.fit(data)
    c = kmeans.cluster_centers_
    x_kmeans = kmeans.predict(data)
    avg_dist = average_dists_from_centers(data, c, x_kmeans)
    print("KMeans avg. cluster distance: \t" + str(avg_dist))
    print("KMeans silhouette score: \t" + str(silhouette_score(data, x_kmeans)))
    print("KMeans jaccard index: \t" + str(jaccard_index(labels, x_kmeans)))
    plt.scatter(c[:,0], c[:,1])

    # Adjacency matrix is computed via the "pairwise_kernels" function, using rbf kernel. This is needed, SC can't be
    # directly applied to the features.
    sc = SpectralClustering(n_clusters=10)
    x_sc = sc.fit_predict(data)
    print("SC silhouette score: \t\t" + str(silhouette_score(data, x_sc)))
    print("SC jaccard index: \t" + str(jaccard_index(labels, x_sc)))

    # The required similarity matrix is computed via the "euclidean_distances" function and negated.
    # Since we are in R², euclidean distances are the most intuitive measure.
    ap = AffinityPropagation(random_state=None)
    ap.fit(data)
    
    c = ap.cluster_centers_
    if len(c) > 0:
        x_ap = ap.predict(data)
        avg_dist = average_dists_from_centers(data, c, x_ap)
        print("AP avg. cluster distance: \t" + str(avg_dist))
        print("AP silhouette score: \t\t" + str(silhouette_score(data, x_ap)))
        print("AP jaccard index: \t" + str(jaccard_index(labels, x_ap)))
        plt.scatter(c[:,0], c[:,1])
    else:
        print("Error! AP did not converge.")

    plt.legend(["Samples", "Centers KMeans", "Centers AP"])
    plt.show()
    
    _, axis = plt.subplots(2, 2)
    
    # Original labels
    sorted = [np.array([d for j, d in enumerate(data) if labels[j] == i]) for i in range(10)]
    for s in sorted:
        if len(s) > 0:
            axis[0, 0].scatter(s[:, 0], s[:, 1])
    axis[0, 0].set_title("Ground Truth")
    
    # Labels according to SC
    sorted_sc = [np.array([d for j, d in enumerate(data) if x_sc[j] == i]) for i in range(10)]
    for s in sorted_sc:
        if len(s) > 0:
            axis[0, 1].scatter(s[:, 0], s[:, 1])
    axis[0, 1].set_title("SC prediction")
        
    if len(c) > 0:
        # Labels according to AP
        sorted_ap = [np.array([d for j, d in enumerate(data) if x_ap[j] == i]) for i in range(10)]
        for s in sorted_ap:
            if len(s) > 0:
                axis[1, 0].scatter(s[:, 0], s[:, 1])
        axis[1, 0].set_title("AP prediction")
        
    # Labels according to KMeans
    sorted_kmeans = [np.array([d for j, d in enumerate(data) if x_kmeans[j] == i]) for i in range(10)]
    for s in sorted_kmeans:
        if len(s) > 0:
            axis[1, 1].scatter(s[:, 0], s[:, 1])
    axis[1, 1].set_title("KMeans prediction")
        
    plt.show()
    

if __name__ == '__main__':
    data1, labels1 = generate_dataset1()
    data2, labels2 = generate_dataset2()
    print("Checkerboard")
    apply(data1, labels1)
    print("-------------------")
    print("Circle")
    apply(data2, labels2)
    